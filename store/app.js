const app = (function() {


  class Product {
    constructor(id, base, filling, topping, coating, glutenfree, price) {
      this.id = id;
      this.base = base;
      this.filling = filling;
      this.topping = topping;
      this.coating = coating;
      this.price = price;
      this.glutenfree = glutenfree;
    }

    static randomizeComponents() {
      var numbers = [];
      //random base
      var randIndex = (Math.floor((Math.random() * compontentManager.bases.length)));
      numbers.push(compontentManager.bases[randIndex].id)
      //random filling
      var randIndex = (Math.floor((Math.random() * compontentManager.fillings.length)));
      numbers.push(compontentManager.fillings[randIndex].id)
      //random topping
      var randIndex = (Math.floor((Math.random() * compontentManager.toppings.length)));
      numbers.push(compontentManager.toppings[randIndex].id)
      //random coating
      var randIndex = (Math.floor((Math.random() * compontentManager.coatings.length)));
      numbers.push(compontentManager.coatings[randIndex].id)
      var gluten = (Math.round(Math.random()));

      $('#baseSelection').val(numbers[0]).trigger('change');
      $('#baseSelection').trigger('select2:select');
      $('#fillingSelection').val(numbers[1]).trigger('change');
      $('#fillingSelection').trigger('select2:select');
      $('#toppingSelection').val(numbers[2]).trigger('change');
      $('#toppingSelection').trigger('select2:select');
      $('#coatingSelection').val(numbers[3]).trigger('change');
      $('#coatingSelection').trigger('select2:select');
      $('#glutenSelection').val(gluten).trigger('change');
      $('#glutenSelection').trigger('select2:select');
    }
  }

  class Customer {
    constructor(JWTtoken, username) {
      this.JWTtoken = JWTtoken;
      this.username = username;
    }
	
	login(_password, _username) {
		var thisUser = this;
		axios.post('http://localhost:8080/login',{
			username : _username,
			password : _password
		})
		.then(function (response) {
			console.log(response)
			console.log(response.headers)
			console.log(response.headers.Authorization);
			var token = response.headers.Authorization;
			thisUser.username = _username
			thisUser.JWTtoken = token
			$("#usernameLoginForm").val('');
			$("#passwordLoginForm").val('');
			$("#loginForm").hide();
			$(".navbar").append(
			"<div class='greeter'>"+
			"<span class='mr-3'>Witaj, "+ thisUser.username +"!</span>"+
			"<button id='logoutButton' class='btn btn-outline-primary' type='button'>Wyloguj</button>"+
			"</div>"
			);
		})
		.catch(function (error) {
			console.log(error);
			$("#usernameLoginForm").val('');
			$("#passwordLoginForm").val('');
			swal({
			title: "Błąd logowania!",
			text: "Podałeś niepoprawne hasło lub/i nazwę użytkownika!",
			icon: "warning",
			dangerMode: true,
		  });
		});
	}
	
	logout() {
		 $('.greeter').remove();
		$("#loginForm").show();
		this.JWTtoken = '';
		this.username = '';
	}
	
	updateData() {
		this.name = $("#name").val();
		this.surname = $("#surname").val();
		this.email = $("#email").val();
		this.city = $("#city").val();
		this.street = $("#street").val();
		this.postalCode = $("#postalCode").val();
	}
	
	placeOrder(order) {
		if(order.length == 0) {
			  swal({
				title: "Puste zamówienie!",
				text: "Przed złożeniem zamówienia dodaj do niego przynajmniej jeden produkt!",
				icon: "warning",
				dangerMode: true,
			  });
		} else {
			//if user is not logged in
			if (this.JWTtoken == '' || this.username == '' || this.JWTtoken == undefined || this.username == undefined ) {
			  $('#dataModal').modal('show')
			} else {
				//Get customer data from API
				//Go to payment
			}
		}
	}
  }

  class ComponentManager {
    constructor() {
      this.bases = []
      this.fillings = []
      this.toppings = []
      this.coatings = []
    }
	
	getComponents() {
	  var mockData = JSON.parse(document.getElementById('mockData').innerHTML);
      this.bases = mockData.bases;
      this.toppings = mockData.toppings;
      this.fillings = mockData.fillings;
	  this.coatings = mockData.coatings;

	  //Get components from DB
	  /*
	  axios.get('/bases/get-all')
	  .then(function (response) {
		var bases = JSON.parse(response);
		this.bases = bases;
	  })
	  .catch(function (error) {
		console.log(error);
	  })
	  */
	  
      $('#baseSelection').select2({
        placeholder: "Wybierz kształt bazy",
        minimumResultsForSearch: -1
      });

      for (var i=0; i<this.bases.length; i++) {
        var data = {
            id: this.bases[i].id,
            text: this.bases[i].name + ' - ' + this.bases[i].price.toFixed(2) + 'zł',
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#baseSelection').append(newOption).trigger('change');
      }

      $('#fillingSelection').select2({
        placeholder: "Wybierz nadzienie",
        minimumResultsForSearch: -1
      });

      for (var i=0; i<this.fillings.length; i++) {
        var data = {
            id: this.fillings[i].id,
            text: this.fillings[i].name + ' - ' + this.fillings[i].price.toFixed(2) + 'zł',
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#fillingSelection').append(newOption).trigger('change');
      }

      $('#toppingSelection').select2({
        placeholder: "Wybierz posypkę",
        minimumResultsForSearch: -1
      });

      for (var i=0; i<this.toppings.length; i++) {
        var data = {
            id: this.toppings[i].id,
            text: this.toppings[i].name + ' - ' + this.toppings[i].price.toFixed(2) + 'zł',
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#toppingSelection').append(newOption).trigger('change');
      }


      $('#coatingSelection').select2({
        placeholder: "Wybierz polewę",
        minimumResultsForSearch: -1
      });

      for (var i=0; i<this.coatings.length; i++) {
        var data = {
            id: this.coatings[i].id,
            text: this.coatings[i].name + ' - ' + this.coatings[i].price.toFixed(2) + 'zł',
        };
        var newOption = new Option(data.text, data.id, false, false);
        $('#coatingSelection').append(newOption).trigger('change');
      }

      $('#glutenSelection').select2({
        placeholder: "Czy bez glutenu?",
        minimumResultsForSearch: -1
      });
	}
  }

  class Order {
    constructor() {
      this.currentId = 0;
      this.products = [];
    }

    get totalPrice() {
      var total = 0;
      for (var i=0; i<order.length; i++) {
        total += this.products[i].price
      }
      return total.toFixed(2)
    }

    get length() {
      return this.products.length
    }

    get id() {
      return this.currentId
    }

    push(product) {
      this.products.push(product);
      this.currentId++;
      $('#totalPrice').html(order.totalPrice);
      $('#totalPriceModal').html(order.totalPrice);
    }

    remove(idToRemove) {
      for (var i=0; i<order.length; i++) {
        if (this.products[i].id == idToRemove) {
          this.products.splice(i,1);
        }
      }
      $('#totalPrice').html(order.totalPrice);
      $('#totalPriceModal').html(order.totalPrice);
    }
  }

  var swiper;
  var order = new Order();
  var customer = new Customer();
  var compontentManager = new ComponentManager();

  $('select').on('select2:select', function (e) {
    var baseId = $("#baseSelection").val();
    var fillingId = $("#fillingSelection").val();
    var toppingId = $("#toppingSelection").val();
    var coatingId = $("#coatingSelection").val();
    var basePrice = findById(compontentManager.bases, baseId).price
    var fillingPrice = findById(compontentManager.fillings, fillingId).price
    var toppingPrice = findById(compontentManager.toppings, toppingId).price
    var coatingPrice = findById(compontentManager.coatings, coatingId).price

    var totalPrice = basePrice + fillingPrice + toppingPrice + coatingPrice;
    $('#totalProductPrice').html(totalPrice.toFixed(2));
    $('#totalPriceModal').html(totalPrice.toFixed(2));

  });

  $("#randomizeButton").on("click",function(){
    Product.randomizeComponents();
  });

  $("#addToCartButton").on("click",function() {
    var baseId = $("#baseSelection").val();
    var fillingId = $("#fillingSelection").val();
    var toppingId = $("#toppingSelection").val();
    var coatingId = $("#coatingSelection").val();
    var glutenfree = $("#glutenSelection").val();
    var base = findById(compontentManager.bases, baseId)
    var filling = findById(compontentManager.fillings, fillingId)
    var topping = findById(compontentManager.toppings, toppingId)
    var coating = findById(compontentManager.coatings, coatingId)
    var totalProductPrice = base.price + filling.price + topping.price + coating.price;

    order.push(new Product(order.id,base,filling,topping,coating,glutenfree,totalProductPrice));

    if(order.length > 0) {
      $(".empty-order").hide();
      $(".swiper-container").show();
    }

    swiper.appendSlide(
      '<div class="swiper-slide">\
      <div class="single-product">\
      <p>Koszt produktu: <span class="product-price">'+totalProductPrice.toFixed(2)+'zł</span></p>\
      <p>Baza: <span class="product-shape">'+base.name+'</span></p>\
      <p>Nadzienie: <span class="product-filling">'+filling.name+'</span></p>\
      <p>Posypka: <span class="product-topping">'+topping.name+'</span></p>\
      <p>Polewa: <span class="product-color">'+coating.name+'</span></p>\
      <p>Bez glutenu: <span class="product-color">'+ (glutenfree == 1 ? 'Tak' : 'Nie') +'</span></p>\
      <button data-index='+(order.id-1)+' class="btn btn-outline-danger remove-button">Usuń</button>\
      </div>\
      </div>'
    )
    swiper.update();
    swiper.slideTo(order.products.length);
  });

  $(document).on('click','.remove-button',function() {
    swiper.removeSlide(swiper.clickedIndex);
    swiper.update();
    var idToRemove = $(this).data("index")
    order.remove(idToRemove)
    if(order.length == 0) {
      $(".swiper-container").hide();
      $(".empty-order").show();
    }
  });

  $("#orderButton").on('click',function() {
	customer.placeOrder(order);
  });

  $(document).on('click','#logoutButton', function() {
	customer.logout();
  });

  $("#loginForm").on("submit", function( event ) {
    event.preventDefault();
    var username = $("#usernameLoginForm").val();
    var password = $("#passwordLoginForm").val();
	customer.login(password, username);
  })
  
  $("#finalizeButton").click(function() {
	  customer.updateData();
  });

  const initApp = function() {
    $(document).ready(function() {

	  compontentManager.getComponents();
      swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
      });
    });

  }

  var findById = function(array, id) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].id == id)
            return array[i]; // Return as soon as the object is found
    }
    return null; // The object was not found
  }

  return {
    init : initApp,
    order : order
  }

})();

app.init();
