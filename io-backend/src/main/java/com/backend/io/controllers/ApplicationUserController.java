package com.backend.io.controllers;

import com.backend.io.model.ApplicationUser;
import com.backend.io.repositories.ApplicationUserRepository;
import com.backend.io.services.AuthorityService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class ApplicationUserController {

    private ApplicationUserRepository applicationUserRepository;
    private AuthorityService authorityService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ApplicationUserController(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                                     AuthorityService authorityService) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authorityService = authorityService;
    }

    @PostMapping("/sign-up/client")
    public void signUp(@RequestBody ApplicationUser user) {
        //System.out.println(user.getPassword());
        user.getAuthorities().add(authorityService.getByName("ROLE_CLIENT"));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
    }
}
