package com.backend.io.services;


import com.backend.io.model.Authority;
import com.backend.io.repositories.AuthorityRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {
    private AuthorityRepository authorityRepository;

    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public Authority getByName(String name){
        return this.authorityRepository.findByName(name);
    }
}