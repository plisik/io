package com.backend.io.authorization;

import java.util.ArrayList;
import java.util.List;

public class LocalPermissionsRepository implements IPermissionsRepository {

	private ArrayList<RolePermissions> rolePermissions;
	
	public LocalPermissionsRepository(){
		rolePermissions = new ArrayList<RolePermissions>();
		
		ArrayList<String> permissions= new ArrayList<String>();		
		permissions.add("rob");
		permissions.add("kup");
		rolePermissions.add(new RolePermissions("CLIENT", permissions));
		permissions= new ArrayList<String>();		
		permissions.add("rob");
		rolePermissions.add(new RolePermissions("KUPUJACY", permissions));
	}

	@Override
	public ArrayList<String> getPermissions(String role) {
		ArrayList<String> permissions = new ArrayList<String>();
		for(int i=0;i<rolePermissions.size();i++){
			if(rolePermissions.get(i).getRole() == role){
				permissions.addAll(rolePermissions.get(i).getPermissions());
			}
		}
		return permissions;
	}
	
	

}
