package com.backend.io.authorization;

import java.util.ArrayList;
import java.util.List;

public class RolePermissions {
	private String role;
	private ArrayList<String> permissions;
	
	public ArrayList<String> getPermissions() {
		return (ArrayList<String>) permissions.clone();
	}

	public String getRole() {
		return role;
	}
	
	public RolePermissions(String role, ArrayList<String> permissions) {
		this.role=role;
		this.permissions=permissions;
	}
	
}
