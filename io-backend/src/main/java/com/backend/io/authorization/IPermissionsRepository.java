package com.backend.io.authorization;

import java.util.List;

public interface IPermissionsRepository {
	public List<String> getPermissions(String role);
}
