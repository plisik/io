package com.backend.io.authorization;

import java.util.ArrayList;
import com.backend.io.repositories.ApplicationUserRepository;
import java.util.List;

import com.backend.io.authorization.IPermissionsRepository;
import com.backend.io.authorization.IUserRoleIdentifier;

public class JWTAuthorizationFilter {
	
	private IPermissionsRepository permissionsRepository;
	private	IUserRoleIdentifier userRoleIdentifier;
	private ApplicationUserRepository applicationUserRepository;

	
	public JWTAuthorizationFilter(IPermissionsRepository permissionsRepository, IUserRoleIdentifier userRoleIdentifier, ApplicationUserRepository applicationUserRepository){
		this.permissionsRepository = permissionsRepository;
		this.userRoleIdentifier = userRoleIdentifier;
		this.applicationUserRepository = applicationUserRepository;
	}
	public JWTAuthorizationFilter(ApplicationUserRepository applicationUserRepository){
		this.permissionsRepository = new LocalPermissionsRepository();
		this.userRoleIdentifier = new DatabaseUserRoleIdentifier();
		this.applicationUserRepository = applicationUserRepository;
	}
	public boolean processRequest(String JWT, String action) {
		List<String> roles=this.userRoleIdentifier.getUserRoles(applicationUserRepository, JWT);
		for (String role : roles) {
			ArrayList<String> permissions=(ArrayList<String>) permissionsRepository.getPermissions(role);
			if (permissions.contains(action)) {
			    return true;
			}
        }
		return false;
	}

	
	
}
