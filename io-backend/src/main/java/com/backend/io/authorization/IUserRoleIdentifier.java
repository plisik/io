package com.backend.io.authorization;

import java.util.List;

import com.backend.io.repositories.ApplicationUserRepository;

public interface IUserRoleIdentifier {
	public List<String> getUserRoles(ApplicationUserRepository applicationUserRepository, String JWT);
}
