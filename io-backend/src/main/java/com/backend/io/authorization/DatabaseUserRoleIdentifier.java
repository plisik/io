package com.backend.io.authorization;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.backend.io.model.ApplicationUser;
import com.backend.io.model.Authority;
import com.backend.io.repositories.ApplicationUserRepository;


public class DatabaseUserRoleIdentifier implements IUserRoleIdentifier{

	private String dataBaseUserName;
	private String dataBaseName;
	private String dataBaseUserPassword;
	private String dataBaseServerIP;
	private String dataBaseServerPort;
	@Override
	public List<String> getUserRoles(ApplicationUserRepository applicationUserRepository, String jwt) {
		try {
			DecodedJWT decodedJwt = JWT.decode(jwt);
			String username = decodedJwt.getSubject();
			ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
	        if (applicationUser == null) {
	            throw new UsernameNotFoundException(username);
	        }
	        List<Authority> userAuthorities=applicationUser.getAuthorities();
	        List<String> userRoles=new ArrayList<String>();
	        for (Authority userAuthority : userAuthorities) {
	        	userRoles.add(userAuthority.getName());
	        }
	        return userRoles;
	        
		} catch (JWTDecodeException exception) {
			//Invalid token
		}
		return null;
	}

}
