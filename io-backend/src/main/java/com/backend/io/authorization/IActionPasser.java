package com.backend.io.authorization;

public interface IActionPasser {
	public void passAction(String action);
}
