package com.backend.io.model;

public enum UserRoleName {
    MANAGER, CLIENT, CUKIERNIK;

    public String authority() {
        return "ROLE_" + this.name();
    }
}
